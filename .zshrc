#source /etc/zsh/zprofile

export HISTFILE=~/.zhistory
export HISTFILESIZE=1000
export HISTSIZE=1000
export SAVEHIST=1000

setopt EXTENDED_HISTORY #time
setopt INC_APPEND_HISTORY_TIME  # append command to history file immediately after execution

# Where to search for commands
PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/home/fezile/Scripts

#PROMPT
if [[ $UID -eq 0 ]]; then
	PS1=[$(whoami)@$HOST]'# '  
else
	PS1=[$(whoami)@$HOST]'$ '
fi

#No need for ls, immediately see directory contents after cd-ing into it
function cdAndls {
	cd "$1"

	if [[ $? -eq 0 ]]; then
		if [[ $(ls|wc -l) -eq 0 ]]; then
			echo
			echo "No files here, Fez."
			echo "Consider looking for hidden files :)"
			echo
		fi
		
		if [[ $(ls|wc -l) -lt 101 ]]; then
			# What's in here?
			ls --color=auto
		fi
	fi
}

function mvAndRefresh {
	mv "$1" "$2"
	#clear: Must be ctrl+l. I can't scroll back after clear-ing the screen
	ls --color=always
}

function touchAndRefresh {
	touch "$1" && ls --color=always
}

alias cd='cdAndls $1'
#alias mv='mvAndRefresh $1 $2'
alias touch='touchAndRefresh $1'
alias ls='ls --color=always'
alias grep='grep --color=always'
alias ll='ls -l'
alias la='ls -a'
bindkey \^U backward-kill-line #CtrlU
